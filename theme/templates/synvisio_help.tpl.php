<?php

/**
 * @file
 * Synvisio administrative help page.
 */
?>

<h3>About Synvisio</h3>
<p>
  Synviso : An Interactive Multiscale Synteny Visualization Tool for McScanX.
</p> 
GitHub: <a href="https://github.com/kiranbandi/synvisio">https://github.com/kiranbandi/synvisio</a><br/>
<br/>
<dl>
  <dt>If you use it, please cite:</dt>
  <dd>
  <a href="https://openreview.net/pdf?id=7-C5VJWbnI">
  Venkat Bandi, and Carl Gutwin. 2020. Interactive Exploration of Genomic Conservation. In Proceedings of the 46th Graphics Interface Conference on Proceedings of Graphics Interface 2020 (GI’20). Canadian Human-Computer Communications Society, Waterloo, CAN.</a></dd>
</dl> 
<br/>

<h3>Synvisio Module for Drupal</h3>
<p>
  This extension module for Drupal <b>does not include Synvisio engine</b>.
  You can get Synvisio from <a href="https://github.com/kiranbandi/synvisio">here</a>.
  This module only provides an easy way to include Synvisio interface into an
  existing Drupal site.
</p>

<h3>Installation and configuration</h3>
Configuration URL: <?php
  echo l(t('Settings'), 'admin/tripal/extension/synvisio');
?>
<p>
First, you must install a Synvisio server and load your data as explained on GitHub <a href="https://github.com/kiranbandi/synvisio/tree/self-hosted-module#readme" target="_blank">Self-hosted</a>
</p>
<p>
  Then, you must configure the <i>Synvisio server URL</i>. It must be the public
  URL to your Synvisio server.
  <br/>
  For instance "//synvisio.myserver.com/"<br/>
  It is possible to use a Synvisio server that is proxy-ed on your PHP web server
  running Drupal in order to have a common server name. This could be possible
  if you run your Drupal site in a subdirectory of your server. For instance,
  your Drupal site is on "https://www.myserver.com/drupal/" and you Synvisio server
  is proxy-ed at "https://www.myserver.com/synvisio/". In this case, you can use
  the Synvisio URL "/synvisio/" instead of "//synvisio.myserver.com".
</p>
<p>
  Once the Synvisio server URL as been set, you will have to add dataset settings.
  Synvisio can search on datasets you loaded. You can provide a limited list of datasets
  for your Drupal users but be aware that other datasets loaded into Synvisio may be
  accessed by advanced users if they know those dataset names or access Synvisio
  server directly (ie. not through your Drupal site).<br/>
  dataset settings are just a convenient way to give human-readable names to your
  datasets and provide an easy dataset selection interface to your users.
</p>
<p>
  Finally, you will have to add a link to your Synvisio page on your site (path:
  "<?php echo l(t('SynVisio'), 'synvisio'); ?>") and add the
  <?php
    echo l(
      t('"Use SynVisio Drupal interface" permission'),
      'admin/people/permissions',
      array('fragment' => 'module-synvisio')
    );
  ?>
  to the appropriate roles (anonymous users, authenticated users, etc.).
</p>

<?php

/**
 * @file
 * Synvisio user interface.
 */
  $settings = variable_get('synvisio', array());

  if (empty($settings)
      || empty($settings['url'])
      || empty($settings['datasets'])) {
?>
    <p>Synvisio has not been configured for this site.</p>
<?php
    return;
  }

  drupal_add_library('system', 'ui.resizable');
  $query_parameters = drupal_get_query_parameters();
  $datasets = array();
  foreach ($settings['datasets'] as $dataset) {
    $datasets[$dataset['machinename']] = $dataset;
  }

  $active_dataset = $settings['default_dataset'] ?: $settings['datasets'][0]['machinename'];
  if (!empty($query_parameters['synvisio_dataset'])
      && array_key_exists($query_parameters['synvisio_dataset'], $datasets)) {
    $active_dataset = $query_parameters['synvisio_dataset'];
  }


  $synvisio_frame_url =
    $settings['url']
    . "#/Dashboard/$active_dataset";
?>
<style>
iframe {
    display: block;       /* iframes are inline by default */
    background: #000;
    border: none;         /* Reset default border */ 
    width: 100vw;
    min-height: 800px;
} 
.container {
    max-width: 100%;
} 
</style>
<form>
  <div id="synvisio_dataset_selector" style="display: none;">
    <select id="synvisio_dataset_selector_select">
      <?php
        foreach ($datasets as $dataset_machinename => $dataset) {
          if ($active_dataset == $dataset_machinename) {
            $selected = ' selected="selected"';
          }
          else {
            $selected = '';
          }
          echo
            "    <option value=\"$dataset_machinename\"$selected>"
            . $dataset['name']
            . "</option>\n";
        }
      ?>
    </select>
  </div>
</form>
<br/>
<div id="synvisio_container">
  <iframe id="synvisio" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%"  src="<?php echo $synvisio_frame_url; ?>">Your browser does not support iframes. Click on this <a src="<?php echo $synvisio_frame_url; ?>">Synvisio link</a> in order to access Synvisio in degraded mode.</iframe>
</div>
<script>
  var synvisio_dataset_settings = <?php echo json_encode($datasets); ?>;

  jQuery(function() {
    var $synvisio_container = jQuery("#synvisio_container");
    var $synvisio = jQuery("#synvisio");
    jQuery("#synvisio_dataset_selector")
      .show()
      .find('> select')
      .on('change', function() {
        // Replaces "Dashboard/..." value in URL.
        var dataset_offset = $synvisio.attr('src').indexOf('Dashboard/') + 10;
        var next_param_offset = $synvisio.attr('src').indexOf('&', dataset_offset);
        if (next_param_offset < 0) {
          next_param_offset = $synvisio.attr('src').length;
        }
        url = $synvisio.attr('src')
          .substr(0, dataset_offset)
            + jQuery("#synvisio_dataset_selector > select").val()
            + $synvisio.attr('src').substr(next_param_offset)
        ;
        $synvisio.attr('src', url);
      })
    ;

    $synvisio_container.resizable();
  });
</script>

<?php

/**
 * @file
 * Contains the functions used for administration of the SynVisio module.
 */

/**
 * Administrative settings form.
 *
 * @return string
 *   the HTML content of the administration form.
 */
function synvisio_admin_form($form, &$form_state, $no_js_use = FALSE) {
  // Get current settings.
  $settings = variable_get('synvisio', array());

  $form = array();

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Synvisio server URL'),
    '#title_display' => 'before',
    '#description' => t('Only include "http" protocol if "https" is not enabled on SynVisio server. Prefer a URL like "://synvisio.server.com/synvisio".'),
    '#default_value' => $settings['url'],
    '#size' => 80,
    '#required' => TRUE,
  );

  $form['datasets'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Synvisio available datasets'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  if (empty($form_state['dataset_count'])) {
    $form_state['dataset_count'] = count($settings['datasets']);
  }

  $form['datasets']['dataset_list'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="synvisio-dataset-fieldset-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  for ($i = 0; $i < $form_state['dataset_count']; ++$i) {
    $form['datasets']['dataset_list'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Dataset settings'),
    );
    $default_value = !empty($settings['datasets'][$i]['machinename']) ?
      $settings['datasets'][$i]['machinename']
      : '';
    $form['datasets']['dataset_list'][$i]['machinename'] = array(
      '#type' => 'textfield',
      '#title' => t('Dataset machine (internal) name'),
      '#title_display' => 'before',
      '#description' => t('Synvisio dataset machine name used in URLs.'),
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 128,
    );
    $default_value = !empty($settings['datasets'][$i]['name']) ?
      $settings['datasets'][$i]['name']
      : '';
    $form['datasets']['dataset_list'][$i]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Dataset name'),
      '#title_display' => 'before',
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 128,
    );

    // @todo: Add radio button for default dataset.
  }

  $form['datasets']['add_dataset'] = array(
    '#type' => 'submit',
    '#value' => t('Add a dataset'),
    '#submit' => array('synvisio_add_dataset_add_one'),
    '#ajax' => array(
      'callback' => 'synvisio_add_dataset_callback',
      'wrapper' => 'synvisio-dataset-fieldset-wrapper',
    ),
  );

  // Save button.
  $form['save_settings'] = array(
    '#type'        => 'submit',
    '#value'       => t('Save configuration'),
    '#submit'      => array('synvisio_admin_form_submit'),
  );

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function synvisio_add_dataset_callback($form, $form_state) {
  return $form['datasets']['dataset_list'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function synvisio_add_dataset_add_one($form, &$form_state) {
  $form_state['dataset_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Saves Synvisio configuration changes.
 */
function synvisio_admin_form_submit($form_id, &$form_state) {
  // Get current settings.
  $settings = variable_get('synvisio', array());

  $settings['url'] = $form_state['values']['url'];

  $settings['datasets'] = array();
  foreach ($form_state['values']['dataset_list'] as $dataset) {
    if (!empty($dataset['machinename'])) {
      $settings['datasets'][] = array(
        'machinename' => $dataset['machinename'],
        'name' => $dataset['name'],
      );
    }
  }

  // Save Synvisio settings.
  variable_set('synvisio', $settings);

  drupal_set_message(t('Synvisio configuration saved.'));
}

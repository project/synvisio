SynVisio Extension module
=========================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Partnership


INTRODUCTION
------------

This extension module for Drupal does not include SynVisio tool. It only
provides an easy way to include SynVisio interface into an existing Drupal site.

SynVisio is an interactive multiscale synteny visualization tool for McScanX.

**Project homepage**: https://synvisio.github.io

**GitHub**: https://github.com/kiranbandi/synvisio

**Publication**: Venkat Bandi, and Carl Gutwin. 2020. Interactive Exploration of
  Genomic Conservation. In Proceedings of the 46th Graphics Interface Conference
  on Proceedings of Graphics Interface 2020 (GI’20). Canadian Human-Computer
  Communications Society, Waterloo, CAN.

For any inquiries about SynVisio please contact "venkat.bandi@usask.ca".


REQUIREMENTS
------------

This module requires an external SynVisio server. You may contact the official
SynVisio website (https://synvisio.github.io/) maintainer
(mail venkat.bandi@usask.ca with the subject "MCSCANX Synteny Tool") to add your
datasets there or you may install your own SynVisio server (self-hosted) and
manage your datasets yourself.

For self-hosting, see:
https://github.com/kiranbandi/synvisio/tree/self-hosted-module#readme

Then SynVisio tool requires two files (a dataset) to run:

  * The simplified gff file that was used as an input for a McScanX query.
  
  * The collinearity file generated as an output by McScanX for the same input
    query.

Those 2 files compose a single dataset and must start with the same name and end
respectively with "_coordinate.gff" and "_collinear.collinearity".
For example: `my_dataset-v1.0_coordinate.gff` and `my_dataset-v1.0_collinear.collinearity`.
Therefore, the dataset name will be the common prefix `my_dataset-v1.0`.
Of course, you may add as many datasets as you need but they all should be in a
same directory of your self-hosted instance (see related documentation).


INSTALLATION
------------

 * Have a running SynVisio server and one or more datasets (see Requirements
   above).

 * Enable the Drupal module as usual.


CONFIGURATION
------------- 

 * Go to the configuration page at /admin/tripal/extension/synvisio

 * Set a "SynVisio server URL". It must be the public URL to your SynVisio
   server. You should not include the protocol (just remove what is before the
   "//" in your URL). The URL should not include a trailing slash.
    For instance "//synvisio.myserver.com/synvisio"
    It is possible to use a SynVisio server that is proxy-ed on your PHP web
    server running Drupal in order to have a common server name. This could be
    possible if you run your Drupal site in a subdirectory of your server. For
    instance, your Drupal site is on "https://www.myserver.com/drupal/" and you
    SynVisio server is proxy-ed at "https://www.myserver.com/synvisio". In this
    case, you can use the SynVisio URL "/synvisio" instead of
    "//synvisio.myserver.com/synvisio".

 * Add at least one dataset name.

 * Add a link to your SynVisio page on your site (path: "synvisio") for your users.

 * Configure the permission "Use SynVisio Drupal interface" at
   "admin/people/permissions#module-synvisio"


MAINTAINERS
-----------

Current Drupal module maintainers:

 * Gaëtan Droc (droc) - https://www.drupal.org/user/2661103
 
 * Valentin Guignon (vguignon) - https://www.drupal.org/user/423148


PARTNERSHIP
-----------
SynVisio was developed at the Human Computer Interaction Lab at the University
of Saskatchewan, Canada to assist genome researchers at the Plant Phenotyping
and Imaging Research Centre, Canada (P2IRC). This tool is part of series of
systems that are currently under development as part of the Theme 3
(Computational Informatics of Crop Phenotype Data) of the P2IRC project.
Contributions are made by:

 * Project Lead: Carl Gutwin

 * System Architect: Venkat Bandi

 * Research Collaborators: Isobel Parkin, Andrew Sharpe, Kirstin Bett and Kevin
   Koh
